package cr.ac.ucr.ecci.eseg.miseguridad;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Iterator;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
public class MainActivity extends AppCompatActivity {
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.texto);
        mTextView.setText("");
        String mString = "Prueba de encriptacion de datos en Android.";
        // cifrado asimetrica con el algoritmo de Rivest, Shamir y Adleman (RSA)
        AlgoritmoRSA(mString);
        // cifrado simetrica con Advanced Encryption Standard
        AlgoritmoAES(mString);
        // Lista de algoritmos
        Algoritmos();
    }

    public void Algoritmos() {
        String result = "\n\n Lista de Algoritmos:";
        // Obtener los providers
        Provider[] mProviders = Security.getProviders();
        for (int p = 0; p < mProviders.length; p++) {
            // obtener los tipos de servicios para cada proveedor
            Set<Object> ks = mProviders[p].keySet();
            Set<String> mServiceTypes = new TreeSet<String>();
            for (Iterator<Object> it = ks.iterator(); it.hasNext(); ) {
                String k = it.next().toString();
                k = k.split(" ")[0];
                if (k.startsWith("Alg.Alias.")) {
                    k = k.substring(10);
                }
                mServiceTypes.add(k.substring(0, k.indexOf('.')));
            }
            int s = 1;
            for (Iterator<String> its = mServiceTypes.iterator();its.hasNext(); ) {
                String stype = its.next();
                Set<String> mAlgorithms = new TreeSet<String>();
                for (Iterator<Object> it = ks.iterator(); it.hasNext(); ) {
                    String k = it.next().toString();
                    k = k.split(" ")[0];
                    if (k.startsWith(stype +".")){
                        mAlgorithms.add(k.substring(stype.length() + 1));
                    }
                    else if (k.startsWith("Alg.Alias." + stype + ".")) {
                        mAlgorithms.add(k.substring(stype.length() + 11));
                    }

                }//ks
                int a = 1;
                for (Iterator<String> ita = mAlgorithms.iterator();ita.hasNext(); ) {
                    result += ("[Proveedor: "+ (p + 1) + ":" + mProviders[p].getName() +"]"+
                            "[Servicio: "+ s + ":" + stype + "]" +
                            "[Algoritmo: "+ a + ":" + ita.next() + "]\n");
                    a++;
                }
                s++;
             }//mServiceTypes






        }//for providers
        mTextView.append("\n\n" + result);
    }//algoritmos


    public void AlgoritmoAES(String mString) {
        String result = "\n\n Cifrado simetrica con Advanced Encryption Standard (AES): \n\n";
        result += "\n\n Texto: " + mString +"\n\n";
        // Secret key spec for 128-bit AES encryption and decryption
        SecretKeySpec mSecretKeySpec =null;
        try
        {
            SecureRandom mSecureRandom = SecureRandom.getInstance("SHA1PRNG");
            mSecureRandom.setSeed(new Random().nextLong());
            KeyGenerator mKeyGenerator = KeyGenerator.getInstance("AES");
            mKeyGenerator.init(128,mSecureRandom);
            mSecretKeySpec = new SecretKeySpec((mKeyGenerator.generateKey()).getEncoded(),"AES");

        }
        catch(Exception e)
        {
            Log.e("AES", "AES secret key spec error.");
        }
        // Encode the original data with AES
        byte[] encodedBytes = null;
        try
        {
            Cipher mCipher = Cipher.getInstance("AES");
            mCipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec);
            encodedBytes = mCipher.doFinal(mString.getBytes());
        }
        catch (Exception e)
        {
            Log.e("AES", "AES encryption error");
        }
        result += "[ENCODED]:\n" + Base64.encodeToString(encodedBytes, Base64.DEFAULT) + "\n";
        // Decode the encoded data with AES
        byte[] decodedBytes = null;
        try
        {
            Cipher mCipher = Cipher.getInstance("AES");
            mCipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec);
            decodedBytes = mCipher.doFinal(encodedBytes);
        }
        catch (Exception e)
        {
            Log.e("AES", "AES descyption error");
        }
        result += "[DECODED]:\n" + new String(decodedBytes) + "\n";
        mTextView.append("\n\n" + result);
    }//AES

    public void AlgoritmoRSA(String mString) {
        String result = "\n\n Cifrado asimetrica con el algoritmo de Rivest, Shamir y Adleman (RSA):\n\n";
        result += "\n\n Texto: " + mString +("\n\n");
        // Generate key pair for 1024-bit RSA encryption and decryption
        Key publicKey = null;
        Key privateKey = null;
        try {
            KeyPairGenerator mKeyPairGenerator = KeyPairGenerator.getInstance("RSA");
                    mKeyPairGenerator.initialize(1024); //1024
                    KeyPair mKeyPair = mKeyPairGenerator.genKeyPair();
                    publicKey = mKeyPair.getPublic();
                    privateKey = mKeyPair.getPrivate();
        } catch (Exception e) {
            Log.e("RSA", "RSA key pair error");
        }
        // Encode the original data with RSA private key
        byte[] encodedBytes = null;
        try {
            Cipher mCipher = Cipher.getInstance("RSA");
                    mCipher.init(Cipher.ENCRYPT_MODE,privateKey);
                    encodedBytes = mCipher.doFinal(mString.getBytes());
        } catch (Exception e) {
            Log.e("RSA", "RSA encryption error");
        }
        // String mEncoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        String mEncoded = new String(encodedBytes);
        result += "[ENCODED]:\n" + mEncoded +"\n";
        // Decode the encoded data with RSA public key
        byte[] decodedBytes = null;
        try {
            Cipher mCipher = Cipher.getInstance("RSA");
                    mCipher.init(Cipher.DECRYPT_MODE,publicKey);
                    decodedBytes = mCipher.doFinal(encodedBytes);
        } catch (Exception e) {
            Log.e("RSA", "RSA decryption error");
        }
        // String mDecoded = Base64.encodeToString(decodedBytes, Base64.DEFAULT);
        String mDecoded = new String(decodedBytes);
        result += "[DECODED]:\n" + mDecoded +"\n";
        mTextView.append("\n\n" + result);
    }//RSA
}//clase




